package user

import (
	"errors"
	"fmt"

	"github.com/google/uuid"
)

var ErrUserExist = errors.New("user already exists")

type User struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	UserID    string `json:"user_id"`
	Password  string `json:"password"`
}

type Users []User

type UserStruct struct {
	file FileUser
	US   *Users
}

func newUser(name, surname, email, password string) *User {
	id := uuid.New()

	return &User{
		FirstName: name,
		LastName:  surname,
		Email:     email,
		UserID:    id.String(),
		Password:  password,
	}
}

func NewUserStruct(fileName string) (*UserStruct, error) {
	fileUser, err := NewFileUser(fileName)
	if err != nil {
		return nil, err
	}
	users, err := fileUser.readFromFile()
	if err != nil {
		return nil, err
	}
	return &UserStruct{
		file: *fileUser,
		US:   &users,
	}, nil
}

func (u *UserStruct) search(user User) bool {
	for _, d := range *u.US {
		con3 := d.Email == user.Email
		if con3 {
			return true
		}
	}
	return false
}

func (u *UserStruct) SignUp(fname, lname, email, pw string) (User, error) {

	us := User{}

	us.FirstName = fname
	us.LastName = lname
	us.Email = email
	us.Password = pw

	exists := u.search(us)

	if exists {
		return User{}, ErrUserExist
	} else {
		newUser := newUser(us.FirstName, us.LastName, us.Email, us.Password)

		*u.US = append(*u.US, *newUser)
		err := u.file.writeToFile(*u.US)
		if err != nil {
			return User{}, err
		}
		return *newUser, nil
	}
}

func (u *UserStruct) SignIn(email, password, fullName string) (User, error) {
	userFound := false
	returnedUser := User{}
	for _, d := range *u.US {
		if d.Email == email && d.Password == password  {
			userFound = true
			returnedUser = d
		}
	}

	if userFound {
		return returnedUser, nil
	} else {
		return returnedUser, fmt.Errorf("user not found")
	}

}
