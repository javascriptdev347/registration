package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
)

var errOpeningFile = errors.New("error while opening the file")

type FileUser struct {
	db *os.File
}

func NewFileUser(fileName string) (*FileUser, error) {
	f, err := os.Open(fileName)
	defer f.Close()
	if err != nil {
		return &FileUser{}, fmt.Errorf("error while creating the file: %w", err)
	}
	return &FileUser{db: f}, nil
}

func (fu *FileUser) readFromFile() (Users ,error) {
	NewUsers := Users{}
	f, err := os.Open(fu.db.Name())
	if err != nil {
		return Users{}, errOpeningFile
	}
	defer f.Close()

	if err := json.NewDecoder(f).Decode(&NewUsers); err != nil && err != io.EOF {
		return Users{}, fmt.Errorf("error while trying to decode into user slice: %w", err)
	}

	return NewUsers, nil

}

func (fu *FileUser) writeToFile(us Users) error {
	os.Remove(fu.db.Name())
	file, err := os.OpenFile(fu.db.Name(), os.O_RDWR|os.O_CREATE, 0660)
	if err != nil {
		return errOpeningFile
	}
	defer file.Close()

	// if err := json.NewEncoder(file).Encode(us); err != nil {
	// 	return fmt.Errorf("error while writing to the user json: %w", err)
	// }
	
	data, err := json.MarshalIndent(us, "", "	")
	if err != nil {
		return fmt.Errorf("error while marshaling: %w", err)
	}
	 
	if _, err := file.Write(data); err != nil {
		return fmt.Errorf("error while writing to the user json: %w", err)
	}
	return nil
}