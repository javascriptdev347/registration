package main

import (
	"errors"
	"flag"
	"html/template"
	"log"
	"net/http"
	"registration/user"
	"strconv"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("static/*.html"))
}

func main() {
	port := flag.Int("port", -1, "specify a port")
	flag.Parse()
	ports := strconv.Itoa(*port)
	static := http.FileServer(http.Dir("./static"))
	http.Handle("/", static)
	http.HandleFunc("/registred", RegistrationHandler)
	http.HandleFunc("/loggedin", LoginHandler)
	http.HandleFunc("/login.html", login)
	log.Fatal(http.ListenAndServe(":"+ ports, nil))
}

func login(w http.ResponseWriter, r *http.Request) {
	tpl.ExecuteTemplate(w, "login.html", nil)
}

func RegistrationHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Fatalln(err)
	}
	file := "data/data.json"
	us, err := user.NewUserStruct(file)
	if err != nil {
		log.Fatalln(err)
	}

	fn := r.FormValue("firstn")
	ln := r.FormValue("lastn")
	pw := r.FormValue("password")
	email := r.FormValue("email")
	_, err = us.SignUp(fn, ln, email, pw)

	if errors.Is(err, user.ErrUserExist) {
		tpl.ExecuteTemplate(w, "signup.html", true)
	} else if err == nil {
		tpl.ExecuteTemplate(w, "welcome.html", r.Form)
	} else {
		log.Fatalln(err)
	}
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Fatalln(err)
	}
	file := "data/data.json"
	us, err := user.NewUserStruct(file)
	if err != nil {
		log.Fatalln(err)
	}

	fullName := r.FormValue("fullname")
	pw := r.FormValue("password")
	email := r.FormValue("email")

	user, err := us.SignIn(email, pw, fullName)

	if err != nil {
		tpl.ExecuteTemplate(w, "login.html", true)
	} else {
		tpl.ExecuteTemplate(w, "welcome.html", user)
	}

}
