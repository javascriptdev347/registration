#Registration

This repository contains simple sign in and sign out web page with backend written in Golang. 
To run and check this web page simple open terminal on you machine and type 

"go run cmd/main.go -port 8080" 

then the project will automatically start running on your machine's 8080 port. To see the web page open your browser and type 

"localhost:8080" 

on the search bar. Then the page should appear. There is no database, but .json file was used as a data holder. 